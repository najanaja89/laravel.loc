<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //region Basic functions
    //    function index(Request $request)
//    {
////        return view('welcome',[
////            'message'=>"Hello from HomeController:Index"
////        ]);
//
////        $book = new Book();
////        $book->name = "Hyperion";
////        $book->year = 1989;
////        $book->save();
//
////        $data = $request->all();
////        $id = $request->get('id', 1);
////        $book = Book::find($id);
////
////        if ($book === null)
////            return response('', 403);
//        return "index";
//    }
//
//    function book(Book $book)
//    {
//        return $book;
//    }
    //endregion

    function index()
    {
        return view('index', [
            'books' => Book::all()
        ]);
    }

    function showBook(Book $book)
    {
        return view('show', [
            'book' => $book
        ]);
    }

    function createBook()
    {
        return view("form");
    }

    protected function rules()
    {
        return [
            'name' => 'required|unique:books',
            'year' => 'required|int'
        ];
    }

    function storeBook(Request $request)
    {
        $request->validate($this->rules());

        $book = new Book($request->except('_token'));
        $book->save();

        return redirect()->route('book.show', $book);
        //return $request->except("_token"); //Возвращать все кроме токена
    }

    function editBook(Book $book)
    {
        return view('form', [
            'book' => $book
        ]);
    }

    function updateBook(Request $request, Book $book)
    {
        $rules = $this->rules();
        //required|unique:book,name,{id}
        $rules['name'] .= ",name,{$book->id}";
        $request->validate($rules);

        $data = $request->except('_token', 'method');
        $book->fill($data);
        $book->save();
        return redirect()->route('book.show', $book);
    }

    function deleteBook(Book $book){
        $book->delete();
        return redirect()->route("index");
    }
}


