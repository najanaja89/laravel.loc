<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'HomeController@index')->name('index');

//createBook
Route::get('/book/create', 'HomeController@createBook')->name('book.create');
Route::post('/book', 'HomeController@storeBook')->name('book.store');

Route::get('/book/{book}', 'HomeController@showBook')->name('book.show');

//updateBook
Route::get('/book/{book}/edit', 'HomeController@editBook')->name('book.edit');
Route::put('/book/{book}', 'HomeController@updateBook')->name('book.update');

//deleteBook
Route::delete('/book/{book}', 'HomeController@deleteBook')->name('book.delete');

