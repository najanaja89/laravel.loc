<form action="{{isset($book) ? route('book.update', $book):route("book.store")}}" method="post">
    @csrf
    @if($book ?? false)
        @method('PUT')
    @endif
    <div>
        <input type="text"
               name="name"
               placeholder="Book name..."
               value="{{old("name") ?? ($book->name ?? "")}}">
        @error('name')
        <div style="color: red">
            {{$message}}
        </div>
        @enderror
    </div>
    <div>
        <input type="number"
               name="year"
               placeholder="Book year..."
               value="{{old("year") ?? ($book->year ?? "")}}">
        @error('year')
        <div style="color: red">
            {{$message}}
        </div>
        @enderror
    </div>
    <button>Save</button>
</form>
